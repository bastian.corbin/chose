# Objectif
Concevoir le type de classe `chose` de façon que le programme
principal suivant fournisse l'affichage ci-après.

## Code du programme principal

```c++
#include <iostream>

int main(){
    chose ma_chose;
    std::cout << "Bonjour" << std::endl;
    return 0;
}
```

## Compilation

```
$ g++ -Wall -o chose main.cc chose.cc
```

## Affichage attendu

```
création d'un objet de type 'chose'
Bonjour
destruction d'un objet de type 'chose'
```

## Analyse avec `valgrind`
`valgrind` permet d'analyser les fuites mémoires lors de l'exécution
d'un programme.

```
$ valgrind ./chose
==239571== Memcheck, a memory error detector
==239571== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==239571== Using Valgrind-3.16.1 and LibVEX; rerun with -h for copyright info
==239571==
==239571== HEAP SUMMARY:
==239571==     in use at exit: 0 bytes in 0 blocks
==239571==   total heap usage: 2 allocs, 2 frees, 73,728 bytes allocated
==239571==
==239571== All heap blocks were freed -- no leaks are possible
==239571==
==239571== For lists of detected and suppressed errors, rerun with: -s
==239571== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```

# Complément
Que fournira alors l'exécution du programme principal suivant ?

```c++
#include <iostream>

int main(){
    chose *ma_chose = new chose;
    std::cout << "Bonjour" << std::endl;
    return 0;
}
```

## Analyse avec `valgrind`
Expliquez pourquoi `valgrind` trouve une fuite mémoire.

```
$ valgrind ./chose
==241886== Memcheck, a memory error detector
==241886== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==241886== Using Valgrind-3.16.1 and LibVEX; rerun with -h for copyright info
==241886==
==241886== HEAP SUMMARY:
==241886==     in use at exit: 1 bytes in 1 blocks
==241886==   total heap usage: 3 allocs, 2 frees, 73,729 bytes allocated
==241886==
==241886== LEAK SUMMARY:
==241886==    definitely lost: 1 bytes in 1 blocks
==241886==    indirectly lost: 0 bytes in 0 blocks
==241886==      possibly lost: 0 bytes in 0 blocks
==241886==    still reachable: 0 bytes in 0 blocks
==241886==         suppressed: 0 bytes in 0 blocks
==241886== Rerun with --leak-check=full to see details of leaked memory
==241886==
==241886== For lists of detected and suppressed errors, rerun with: -s
==241886== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```
